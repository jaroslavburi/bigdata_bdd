# -*- coding: utf-8 -*-
import os
import shutil

from behave.log_capture import capture

from steps.Settings import *
from steps.helpers.spark.Spark import Spark


BEHAVE_DEBUG_ON_ERROR = False


def setup_debug_on_error(userdata):
    global BEHAVE_DEBUG_ON_ERROR
    BEHAVE_DEBUG_ON_ERROR = userdata.getbool("BEHAVE_DEBUG_ON_ERROR")


@capture
def before_all(context):
    setup_debug_on_error(context.config.userdata)
    try:
        os.mkdir(Settings().LOGS)
    except FileExistsError:
        pass


def after_step(context, step):
    if BEHAVE_DEBUG_ON_ERROR and step.status == "failed":
        input('Something went wrong; the tests are paused to enable investigation. Hit Enter to continue...')


@capture
def before_scenario(context, scenario):
    context.spark = Spark(timestamp_add=0)
    context.spark.read_parquet('resources/p2p.parquet')

    paths = [attr for attr in Settings().__dict__.values()]
    for path in paths:
        if os.path.isdir(path) and isinstance(path, str) and path != Settings().LOGS:
            shutil.rmtree(path)

    scenario_name = scenario.name.replace(" ", "_")
    context.scenario_name = scenario_name
    spark_log_path = f"{Settings().LOGS}/spark_{scenario_name}.log"
    context.spark_log_handle = open(spark_log_path, 'w+')
    context.spark_log_path = spark_log_path


@capture
def after_scenario(context, scenario):
    context.spark_log_handle.close()

    if scenario.status != "Status.passed":
        paths = [attr for attr in Settings().__dict__.values()]
        for path in paths:
            if os.path.isdir(path) and isinstance(path, str) and path != Settings().LOGS:
                shutil.copytree(path, f"{context.scenario_name}_{path}")
