# -*- coding: utf-8 -*-
import re
from collections import defaultdict


class Utility:

    @staticmethod
    def split_date_remove_zero(date):
        d = date.split("-")
        for i, part in enumerate(d):
            if part[:1] == "0":
                d[i] = part[1:]

        return d

    @staticmethod
    def compare_parquets_rows(parquets, rows):
        """ Compare parquets from HDFS with provided expected results.

        :param parquets: list of parquet files from HDFS
        :param rows: table of expected data
        :return dictionary with k-v pairs where keys are placeholder numbers and values their respective parquet values
        Notes:
        - asterisk '*' is placeholder for _any_ value
        - value like '{1}' means there should be the same value in rows with the same number in curly braces
          (can be used e.g. for semi-randomly generated sessionID checking)
          number of such placeholder should be unique per step unless the actual value in parquet is considered to be same
        """
        Utility._compare_parquets_headings(parquets, rows)
        placeholders_matches = []
        for row in rows:
            row_passed = False

            for parquet in parquets:
                dict_p = parquet.asDict()

                all_keys_match = Utility._inspect_parquet_values(dict_p, row)

                if all_keys_match:
                    Utility._capture_placeholders_values(dict_p, row, placeholders_matches)
                    parquets.remove(parquet)
                    row_passed = True
                    break

            if not row_passed:
                print("Searching for row in results FAILED:")
                print(row)
                print("Remaining parquets:")
                for parquet in parquets:
                    print(parquet.asDict())
                Utility.print_parquets_table(parquets)

            assert row_passed

        dict_matches = Utility._check_parquets_placeholders(placeholders_matches)

        return dict_matches

    @staticmethod
    def print_parquets_table(parquets):
        """ Gherkin-style pretty print for list of parquet files.

        :param parquets:
        """
        print("-" * 80)
        headings = list(parquets[0].asDict().keys())
        print("| ", end="")
        for heading in headings:
            print(heading, end=" | ")
        print("")
        for parquet in parquets:
            print("| ", end="")
            for value in list(parquet.asDict().values()):
                print(value, end=" | ")
            print("")

    @staticmethod
    def _compare_parquets_headings(parquets, rows):
        """ Compare headings of parquet files with expected result.

        :param parquets: list of parquet files from HDFS
        :param rows: table of expected data
        """
        for parquet in parquets:
            dict_p = parquet.asDict()
            headings = [Utility._translate_shortcuts(key) for key in rows.headings]
            if sorted(list(dict_p.keys())) != sorted(headings):
                print("Headings not matching")
                print("These are expected headings missing in output parquet:", list(set(headings) - set(list(dict_p.keys()))))
                print("These are actual headings missing in expected results table:", list(set(list(dict_p.keys())) - set(headings)))
            assert sorted(list(dict_p.keys())) == sorted(headings)

    @staticmethod
    def _inspect_parquet_values(dict_p, row):
        """ Try to find all values of given row (except wildcards "*" and placeholders "{<n>}") in parquet.

        :param dict_p: dictionary representation of one parquet file
        :param row: one row of expected results table
        :return: True if all values are found, otherwise False.
        """
        all_keys_match = True
        for key in row.headings:
            row_value = Utility._prepare_data(row, key)
            parquet_value = Utility._prepare_data(dict_p, key, True)
            if row_value != parquet_value and row[key] != '*' and not re.match(r"\{[0-9]+\}", row[key]):
                all_keys_match = False
                break

        return all_keys_match

    @staticmethod
    def _capture_placeholders_values(dict_p, row, placeholders_matches):
        """ Capture all parquet values for respective placeholders (if any) and extend list of placeholders already found.

        :param dict_p: dictionary representation of one parquet file
        :param row: one row of expected results table
        :param placeholders_matches: list of tuples (k, v) where 'k' is placeholder number and 'v' is list of respective
                                     parquet values found in output parquets
        :return: placeholder_matches extended by newly captured placeholder values
        """
        for key in row.headings:
            parquet_value = dict_p[Utility._translate_shortcuts(key)]
            if re.match(r"\{[0-9]+\}", row[key]):
                match = re.match(r"\{([0-9]+)\}", row[key]).group(1)
                placeholders_matches.append((match, parquet_value))

        return placeholders_matches

    @staticmethod
    def _check_parquets_placeholders(placeholders_matches):
        """ Check that all parquet values for given placeholder are the same.

        :param placeholders_matches: list of tuples (k, v) where 'k' is placeholder number and 'v' is list of respective
                                     parquet values found in output parquets
        :return: dictionary with k-v pairs where keys are placeholder numbers and values their respective parquet values
        """
        dict_matches = defaultdict(set)
        for k, v in placeholders_matches:
            dict_matches[k].add(str(v))
        for k in dict(dict_matches):
            if len(dict(dict_matches)[k]) != 1:
                print("Parquet values for table placeholder \"{{{}}}\" don\'t match".format(k))
            assert len(dict(dict_matches)[k]) == 1

        return dict(dict_matches)

    @staticmethod
    def compare_parquets_session_clicks(parquets, session_id, rows):
        """ Compare clicks produced by shopping session metrics (either brand or domain session) with provided expected results.

        :param parquets: list of parquet files from HDFS
        :param session_id: sessionId from the output parquet
        :param rows: table of expected data for 'clicks'; Note: asterisk '*' is placeholder for _any_ value
        """
        session_id = session_id.pop()
        clicks = []
        for parquet in parquets:
            if parquet.asDict()['sessionId'] == session_id:
                clicks.extend(parquet.asDict()['clicks'])

        # check that all clicks from nth parquet are found in expected results
        # also headings (keys) are checked here, i.e. all clicks must have expected keys
        for click in clicks:
            click = click.asDict()
            if sorted(list(click.keys())) != sorted(rows.headings):
                print("Headings for clicks not matching")
                print("Expected: ",  sorted(rows.headings))
                print("Available: ", sorted(list(click.keys())))

                assert sorted(list(click.keys())) == sorted(rows.headings)

            row_found = False
            if any(row.cells == [str(x) for x in list(click.values())] for row in rows):
                row_found = True
            else:
                print("Some actual click not found in expected results. These are actual clicks:", clicks)

            assert row_found

        # check that all rows of expected results are found in existing output parquet['clicks']
        for row in rows:
            click_found = False
            if any(row.cells == [str(x) for x in list(click.asDict().values())] for click in clicks):
                click_found = True
            else:
                print("Some expected clicks not found in actual clicks. These are actual clicks:", clicks)

            assert click_found

    @staticmethod
    def _prepare_data(row, key, transform=False):
        if transform:
            key = Utility._translate_shortcuts(key)

        data = row[key]
        if not isinstance(data, str):
            data = str(data)

        return sorted(data.replace(" ", ""))

    @staticmethod
    def _translate_shortcuts(key):
        if key == "e":
            return "example"
        else:
            return key
