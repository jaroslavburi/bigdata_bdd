# -*- coding: utf-8 -*-
import os

from steps.Settings import *

s = Settings()


class SparkRunner:

    def __init__(self, context):
        self.spark_log_path = context.spark_log_path

    def run_computation(self, date_from, date_to):
        # from :: to :: outputPath
        args = f"""{date_from} {date_to} {s.OUT_PATH}"""
        self.run_spark(args, "com.example.Runner")

    def run_spark(self, args, metric, debug=False):
        if debug:
            debug_m = f" 2>&1 | tee -a {self.spark_log_path}"
        else:
            debug_m = f" >> {self.spark_log_path} 2>&1"

        bash_command = f""" spark-submit
        --class {metric}
        --master local
        --deploy-mode client
        --conf spark.sql.parquet.binaryAsString=true
        --conf spark.sql.parquet.compression.codec=snappy
        --conf spark.network.timeout=480
        --conf spark.shuffle.blockTransferService=nio
        --conf spark.shuffle.service.enabled=true
        --conf spark.sql.caseSensitive=false
        --conf spark.serializer=org.apache.spark.serializer.KryoSerializer
        --conf spark.sql.shuffle.partitions=1
        --conf spark.dynamicAllocation.enabled=true
        --conf spark.dynamicAllocation.maxExecutors=150
        --conf spark.dynamicAllocation.minExecutors=1
        --conf spark.driver.memory=1g
        --conf spark.driver.memoryOverhead=1g
        --conf spark.executor.memory=2g
        --conf spark.executor.cores=2
        --conf spark.locality.wait=5s
        --conf spark.executor.memoryOverhead=1g
        --conf spark.app.name="{metric}"
        {jar} {args}
        {debug_m}
        """.replace("\n", "")

        exit_code = os.system(bash_command)
        if exit_code != 0:
            print(f"The exit code was {exit_code}")
        assert exit_code == 0

