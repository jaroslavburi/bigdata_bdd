# -*- coding: utf-8 -*-
import os
import time
import random
import string
from datetime import datetime, timedelta
from fnmatch import fnmatch
from pyspark.sql import SparkSession, SQLContext
from pyspark.sql.functions import lit

from .DataStruct import *
from ..Utility import *
from steps.Settings import *


class Spark:

    def __init__(self, timestamp_add=0):
        if os.environ.get('SPARK_HOME') is not None:
            self.spark_home_env = os.environ["SPARK_HOME"]
            del os.environ['SPARK_HOME']

        self.session = SparkSession.builder \
            .master('local') \
            .appName('tests') \
            .config('spark.executor.memory', '6gb') \
            .config('spark.driver.memory', '4000m') \
            .config("spark.cores.max", "2") \
            .config("spark.debug.maxToStringFields", "100") \
            .config("spark.sql.shuffle.partitions", "1") \
            .config("spark.executor.cores", "1") \
            .getOrCreate()

        self.sqlContext = SQLContext(self.session.sparkContext)

        self.timestamp_add = timestamp_add
        self.spark_home_env = None
        self.parquet = None

    def read_parquet(self, path):
        self.parquet = self.sqlContext.read.parquet(path)

    def create_dataframe(self, array, path, date_path=""):
        df = self.session.createDataFrame(array)
        df.write.parquet(path + date_path)


    def update_dataframe(self, table, date, out, search=""):
        if os.environ.get('SPARK_HOME') is not None:
            os.environ["SPARK_HOME"] = self.spark_home_env

        final_parquet = None
        p = self.parquet.limit(1)
        p = p.withColumn("ymd", lit(date.replace("-", "")))

        headings = None
        for row in table:
            headings = row.headings
            # for heading in headings:
            #     p = self.with_column_if_exists(p, row, heading)

            if final_parquet is None:
                final_parquet = p
            else:
                final_parquet = final_parquet.union(p)

        if "timestamp" not in headings:  # set timestamp automatically
            final_parquet = self.update_timestamp(date, final_parquet)

        if search != "":
            search = "search_engine=" + search

        if out == Settings().KEYWORD_PATH:
            d = date.split("-")
        else:
            d = Utility.split_date_remove_zero(date)

        test_parquet_path = out + "/year={0}/month={1}/day={2}/".format(d[0], d[1], d[2]) + search

        final_parquet.write.parquet(test_parquet_path)
        return test_parquet_path

    def update_timestamp(self, date, parquet):
        extra_time = 10000
        timestamp = int(time.mktime(datetime.strptime(date, "%Y-%m-%d").timetuple())) + extra_time + self.timestamp_add
        timestamp *= 1000  # s to ms
        self.timestamp_add += 1
        return parquet.withColumn("timestamp", lit(timestamp))

    def update_date(self, source_path, dest_path, date):
        p = self.sqlContext.read.parquet(source_path)
        p = p.withColumn("ymd", lit(date.replace("-", "")))
        p = self.update_timestamp(date, p)
        # p = p.withColumn("approximateFunnelId", lit(date))
        # p = p.withColumn("guid", lit(date))
        p.write.parquet(dest_path)

    def _get_list_of_days(self, start_date, end_date):
        """ Return list of datetime objects (days) from 'start_date' to 'end_date'.

        :param start_date: first day of the output range (format YYYY-mm-dd)
        :param end_date: last day of the output range (format YYYY-mm-dd)
        :return: list of datetime objects (days) from 'start_date' to 'end_date'.
        """
        start_date = datetime.strptime(start_date, '%Y-%m-%d') - timedelta(days=1)
        end_date = datetime.strptime(end_date, '%Y-%m-%d')
        days = (end_date - start_date).days
        date_list = [end_date - timedelta(days=x) for x in range(0, int(days))]

        return date_list

    def copy_parquets(self, copy_path_dir, start_date, end_date, out_path, just_copy=True):
        """ Copy source parquet files into date-based directories

        :param copy_path_dir: path to dir containing source (template) parquet files
        :param start_date: first day of the output range (format YYYY-mm-dd)
        :param end_date: last day of the output range (format YYYY-mm-dd)
        :param out_path:
        :param just_copy: if set, the method doesn't change content of the parquet files, otherwise it updates the dates

        Note: both start_date and end_date days are _included_ in range of output directories
        """
        date_list = self._get_list_of_days(start_date, end_date)

        copy_paths = []
        from shutil import copyfile
        for file in os.listdir(copy_path_dir):
            if ".parquet" in file and ".crc" not in file:
                copy_paths.append(os.path.join(copy_path_dir, file))

        for date in date_list:
            date = f"{date:%Y-%m-%d}"
            if out_path == Settings().P2P_PATH:
                d = Utility.split_date_remove_zero(date)
            else:
                d = date.split("-")

            path = out_path + f"/year={d[0]}/month={d[1]}/day={d[2]}/"

            for copy_path in copy_paths:
                filename = "parquet_{}.parquet".format(''.join([random.choice(string.ascii_lowercase) for _ in range(32)]))
                if just_copy:
                    os.makedirs(path, exist_ok=True)
                    copyfile(copy_path, path + filename)
                else:
                    self.update_date(copy_path, path + filename, date)

    def load_result_parquets(self, out=Settings().OUT_PATH):
        """ Load result parquets from out path.

        :param out: output path
        :return: list of parquets
        """
        parquets = []
        pattern = "*.parquet"

        for path, subdirs, files in os.walk(out):
            for name in files:
                if fnmatch(name, pattern):
                    parquets.append(self.sqlContext.read.parquet(os.path.join(path, name)))

        flat_parquets = []
        for parquet in parquets:
            for elem in parquet.collect():
                flat_parquets.append(elem)

        return flat_parquets

    def wait_for_result_parquets(self, expected_length, out=Settings().OUT_PATH, retries=5, sleep_time=5):
        """ Wait until result parquet files have expected length. If the length is exceeded or timeout reached it fails.

        :param expected_length:
        :param out: output_path
        :param retries:
        :param sleep_time:
        :return: list of parquets

        See 'load_result_parquets' for more details about parquets.
        """
        parquets = self.load_result_parquets(out)

        counter = 0
        while len(parquets) != expected_length:
            print("length not matching ...")
            parquets = self.load_result_parquets(out)
            if (counter > retries) or (len(parquets) > expected_length):
                print("Length is not matching: actual length is " + str(len(parquets)) + " but expected length was " + str(expected_length))
                for parquet in parquets:
                    print(parquet)
                Utility.print_parquets_table(parquets)
                assert len(parquets) == expected_length

            time.sleep(sleep_time)
            counter += 1

        return parquets

    def calibration_create_one_day(self, rows, date):
        """ Create calibration for one day.

        :param rows:
        :param date:
        :return: directory containing calibration parquet file
        """
        d = date.split("-")

        self.create_dataframe(DataStruct.calibration(rows, date),
                              Settings().CALIBRATION_PATH,
                              "/year={0}/month={1}/day={2}/".format(d[0], d[1], d[2]))
        return Settings().CALIBRATION_PATH + "/year={0}/month={1}/day={2}/".format(d[0], d[1], d[2])

    def calibration_create_range(self, rows, start_date, end_date):
        """ Create calibration for date range.

        :param rows:
        :param start_date:
        :param end_date:
        """
        date_list = self._get_list_of_days(start_date, end_date)
        for date in date_list:
            self.calibration_create_one_day(rows, date.strftime("%Y-%m-%d"))

    def filter_create(self, rows):
        """ Create filter according to provided table.

        :param rows:
        """
        self.create_dataframe(DataStruct.filter(rows), Settings().FILTERS_PATH)
