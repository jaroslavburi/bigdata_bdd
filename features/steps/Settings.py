# -*- coding: utf-8 -*-


class Settings:

    def __init__(self):
        self.DOMAIN_ONLY = False
        self.LOGS = "logs"
        self.TEMP_PATH = "temp"
        self.OUT_PATH = "out"
