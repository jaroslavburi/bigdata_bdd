# -*- coding: utf-8 -*-
from behave import given, when, then

from helpers.spark.SparkRunner import *
from helpers.spark.Spark import *
from helpers.Utility import *
from Settings import *


@given('Initialize spark')
def init_spark(context):
    context.spark = Spark()


@given('I load parquet {parquet}')
def load_parquet(context, parquet):
    context.spark.read_parquet('resources/{0}.parquet'.format(parquet))


@when('I run spark from {date_from} to {date_to}')
def run_spark(context, date_from, date_to):
    SparkRunner.run_spark()


@then('there should be no results')
def no_results(context):
    parquets = context.spark.load_result_parquets(Settings().OUT_PATH)
    assert len(parquets) == 0


@then('I validate results')
def validate_data(context):
    parquets = context.spark.wait_for_result_parquets(len(context.table.rows))
    context.placeholders = Utility.compare_parquets_rows(parquets, context.table)

@when('I set following source data at {date}')
def set_data(context, date):
    context.parquet_path = context.spark.update_dataframe(context.table, date, Settings().P2P_PATH)
