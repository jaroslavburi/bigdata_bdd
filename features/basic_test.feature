Feature: Basic test

  Scenario: Basic test - run spark and verify data
    When I set following source data at 2018-01-01
      | data    |
      | example |
    And I run spark from 2018-01-01 to 2018-01-01
    Then I validate results
      | data    |
      | example |
