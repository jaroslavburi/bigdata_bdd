FROM spark-test:v1

#Build
RUN mkdir -p /opt/bigdata_bdd
WORKDIR /opt/bigdata_bdd

COPY  ./test/resources/ /opt/test/resources
COPY  ./test/features/ /opt/test/features

ENTRYPOINT ["bash", "-c"]
WORKDIR /opt/test
RUN mkdir logs
